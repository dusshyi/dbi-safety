﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DBI.Safety.Models
{
    public class SOCForm
    {
        public List<QuestionsFormViewModel> Questions { get; set; }
        [Required]
        public string Comments { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string Observer { get; set; }
        public DateTime TimeStamp { get; set; }        
        [DisplayName("Task/Activity observed")]
        [Required]
        public string Task { get; set; }
        public IEnumerable<ObserverType> ObserverType { get; set; }
        [Required]
        public int? SelectedObserverType { get; set; }
        public bool IsQuestionSelected { get; set; }
        public bool IsLocationEnabled { get; set; }
    }

    public class ObserverType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
