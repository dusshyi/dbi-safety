﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class SOCViewModel
    {
        public List<QuestionsViewModel> Questions { get; set; }
        [DisplayName("Comments")]
        public string Comments { get; set; }
        [DisplayName("Location")]
        public string Location { get; set; }
        [DisplayName("Observer")]
        public string Observer { get; set; }
        [DisplayName("Entry Date")]
        public DateTimeOffset CreatedDate { get; set; }
        [DisplayName("Task/Activity observed")]
        public string Task { get; set; }
        [DisplayName("Type of observation")]
        public string ObservedBy { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
    }
}
