﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class AddDictionaryForm
    {
        [DisplayName("Category")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your category")]
        public int SelectedCategoryId { get; set; }
        public IEnumerable<SelectListItem> CategoryList { get; set; }        

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayName("Sort Order")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SortOrderId { get; set; }
    }
}
