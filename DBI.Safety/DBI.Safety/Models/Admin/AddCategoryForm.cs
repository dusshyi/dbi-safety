﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class AddCategoryForm
    {
        [DisplayName("Form")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please select your form")]
        public int SelectedFormId { get; set; }
        public IEnumerable<SelectListItem> FormList { get; set; }        

        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [DisplayName("Sort Order")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int SortOrderId { get; set; }
    }
}
