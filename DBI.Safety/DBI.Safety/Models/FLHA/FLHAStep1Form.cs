﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAStep1Form
    {
        public List<FLHADictionaryViewModel> EnvironmentalHazards { get; set; }
        public List<FLHADictionaryViewModel> ErgonomicHazards { get; set; }
        public List<FLHADictionaryViewModel> AccessAndOperationalHazards { get; set; }
        public bool? IsHazardSelected { get; set; }
    }   
}
