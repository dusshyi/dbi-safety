﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAStep2Form
    {
        public List<HazardForm> HazardForms { get; set; }
        [DisplayName("Work to be done")]
        public string Work { get; set; }
        [DisplayName("Task Location")]
        public string Location { get; set; }
        [DisplayName("Muster Point")]
        public string MusterPoint { get; set; }
        [DisplayName("Permit Job")]
        public string PermitJob { get; set; }
        [DisplayName("PPE Inspected")]
        public string PPEInspected { get; set; }
        [DisplayName("Has a pre-use inspection of tools/equipment been completed?")]
        public bool IsToolsAndEquipmentCompleted { get; set; }
        [DisplayName("Warning ribbon needed?")]
        public bool IsWarningRibbonNeeded { get; set; }
        [DisplayName("Is the worker working alone?")]
        public bool IsWorkerWorkingAlone { get; set; }
        public string Explain { get; set; }
        public IEnumerable<YesOrNo> YesOrNo { get; set; }
        public IEnumerable<SelectListItem> SeverityInfo { get; set; }
        public IEnumerable<SelectListItem> ProbabilityInfo { get; set; }
        public bool IsLocationEnabled { get; set; }
    }

    public class YesOrNo
    {
        public bool IsSelected { get; set; }
        public string Name { get; set; }
    }
}
