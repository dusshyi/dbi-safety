﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class FLHAViewModel
    {
        public List<HazardViewModel> Hazards { get; set; }
        [DisplayName("Work to be done")]
        public string WorkInfo { get; set; }
        [DisplayName("Task Location")]
        public string Location { get; set; }
        [DisplayName("Muster Point")]
        public string MusterPoint { get; set; }
        [DisplayName("Permit Job #")]
        public string PermitJobNumber { get; set; }
        [DisplayName("PPE Inspected")]
        public string PPEInspected { get; set; }
        [DisplayName("Has a pre-use inspection of tools/equipment been completed?")]
        public bool IsToolsEquipmentCompleted { get; set; }
        [DisplayName("Warning ribbon needed?")]
        public bool IsWarningRibbonNeeded { get; set; }
        [DisplayName("Is the worker working alone?")]
        public bool IsWorkerWorkingAlone { get; set; }
        [DisplayName("Explain")]
        public string WorkerComment { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
        [DisplayName("Entry Date")]
        public DateTimeOffset CreatedDate { get; set; }
    }
}
