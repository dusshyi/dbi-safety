﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DBI.Safety.Models
{
    public class HazardForm
    {
        public int HazardId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }        
        public string Priority { get; set; }
        public string Plans { get; set; }       

        [DisplayName("Severity")]
        public IEnumerable<SelectListItem> Severity { get; set; }
        public int SelectedSeverity { get; set; }

        [DisplayName("Probability")]
        public IEnumerable<SelectListItem> Probability { get; set; }
        public int SelectedProbability { get; set; }
    }    
}
