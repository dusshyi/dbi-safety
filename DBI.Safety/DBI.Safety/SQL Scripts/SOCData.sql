﻿DROP TABLE SOCDataV2;

CREATE TABLE SOCDataV2 (
	Id INT IDENTITY(1,1) PRIMARY KEY,
	Comments VARCHAR(MAX) NOT NULL,
	[Location] VARCHAR(256) NOT NULL,
	Observer VARCHAR(256) NOT NULL,
	Task VARCHAR(256) NOT NULL,
	ObservedBy VARCHAR(256) NOT NULL,
	Latitude Decimal(9,6) NOT NULL,
	Longitude Decimal(9,6) NOT NULL,
	Accuracy INT NOT NULL,
	IsDeleted BIT NOT NULL DEFAULT 0,
	CreatedBy VARCHAR(256) NOT NULL DEFAULT SYSTEM_USER,
	CreatedDate DATETIMEOFFSET NOT NULL DEFAULT GETDATE(),	
	[Timestamp] DATETIMEOFFSET NOT NULL DEFAULT GETDATE()
);