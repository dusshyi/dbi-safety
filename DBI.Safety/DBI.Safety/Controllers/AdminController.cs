﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DBI.Safety.Controllers
{
    public class AdminController : Controller
    {
        private readonly IConfiguration configuration;
        public AdminController(IConfiguration config)
        {
            this.configuration = config;
        }

        public IActionResult AddForm()
        {
            ViewData["Message"] = "Add new form";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddForm(AddSavetyForm form)
        {
            ViewData["Message"] = "Add new form";

            if (ModelState.IsValid)
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Form (Name, Description, IsActive, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@name, @description, @isActive, @createdBy, @createdDate, @timestamp)", new
                            {
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }

            return View(form);
        }

        public IActionResult AddCategory()
        {
            ViewData["Message"] = "Add new category";

            AddCategoryForm form = new AddCategoryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItem> formList = dbConnection.Query<SelectListItem>(
                    @"SELECT Id AS Value, Name AS Text, [Description]
                    FROM Form
                    WHERE IsActive = 1"
                ).ToList();

                formList.Insert(0, new SelectListItem()
                {
                    Text = "Please select form",
                    Value = 0
                });

                form.FormList = formList;

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCategory(AddCategoryForm form)
        {
            ViewData["Message"] = "Add new category";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");

            if (ModelState.IsValid)
            {                
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Category (FormId, Name, Description, IsActive, SortOrderId, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@formId, @name, @description, @isActive, @sortOrderId, @createdBy, @createdDate, @timestamp)", new
                            {
                                @formId = form.SelectedFormId,
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @sortOrderId = form.SortOrderId,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }

            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItem> formList = dbConnection.Query<SelectListItem>(
                    @"SELECT Id AS Value, Name AS Text, [Description]
                    FROM Form
                    WHERE IsActive = 1"
                ).ToList();

                formList.Insert(0, new SelectListItem()
                {
                    Text = "Please select form"
                });

                form.FormList = formList;

                return View(form);
            }
        }

        public IActionResult AddDictionary()
        {
            ViewData["Message"] = "Add new dictionary";

            AddDictionaryForm form = new AddDictionaryForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItem> categoryList = dbConnection.Query<SelectListItem>(
                    @"SELECT Id AS Value, [Description] + ' - ' + Name AS Text, [Description]
                    FROM Category
                    WHERE IsActive = 1
                    ORDER BY [Description], SortOrderId"
                ).ToList();

                categoryList.Insert(0, new SelectListItem()
                {
                    Value = 0,
                    Text = "Please select category"
                });

                form.CategoryList = categoryList;

                return View(form);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddDictionary(AddDictionaryForm form)
        {
            ViewData["Message"] = "Add new dictionary";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");

            if (ModelState.IsValid)
            {
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            dbConnection.Execute(@"INSERT INTO Dictionary (CategoryId, Name, Description, IsActive, SortOrderId, [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@categoryId, @name, @description, @isActive, @sortOrderId, @createdBy, @createdDate, @timestamp)", new
                            {
                                @categoryId = form.SelectedCategoryId,
                                @name = form.Name,
                                @description = form.Description,
                                @isActive = 1,
                                @sortOrderId = form.SortOrderId,
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans);

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }
            using (IDbConnection dbConnection = conn)
            {
                List<SelectListItem> categoryList = dbConnection.Query<SelectListItem>(
                    @"SELECT Id AS Value, [Description] + ' - ' + Name AS Text, [Description]
                    FROM Category
                    WHERE IsActive = 1
                    ORDER BY [Description], SortOrderId"
                ).ToList();

                categoryList.Insert(0, new SelectListItem()
                {
                    Value = 0,
                    Text = "Please select category"
                });

                form.CategoryList = categoryList;

                return View(form);
            }
        }
    }
}