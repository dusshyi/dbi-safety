﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DBI.Safety.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.ComponentModel;
using DBI.Safety.Helper;

namespace DBI.Safety.Controllers
{
    public class SOCController : Controller
    {
        private readonly IConfiguration configuration;
        public SOCController(IConfiguration config)
        {
            this.configuration = config;
        }

        public SOCForm GetSOCForm(SOCForm activeForm = null)
        {
            SOCForm form = new SOCForm();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var questions = dbConnection.Query<QuestionsFormViewModel>(@"SELECT d.Id, d.Name, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Description = 'Question' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY c.SortOrderId, d.SortOrderId");
                form.Questions = new List<QuestionsFormViewModel>();

                foreach (var item in questions)
                {
                    var possibleAnswers = dbConnection.Query<dynamic>(
                    @"SELECT d.Id, d.Name AS Text
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Name = 'Answer' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").ToList();

                    var possibleAnswerList = new List<Answer>();
                    foreach (var aItem in possibleAnswers)
                    {
                        possibleAnswerList.Add(new Answer { Id = aItem.Id, Text = aItem.Text });
                    }

                    if (activeForm != null)
                    {
                        foreach (var q in activeForm.Questions)
                        {
                            if(item.Id == q.Id)
                            {
                                form.Questions.Add(new QuestionsFormViewModel
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Category = item.Category,
                                    SortOrderId = item.SortOrderId,
                                    PossibleAnswers = possibleAnswerList,
                                    SelectedAnswer = q.SelectedAnswer
                                });
                                break;
                            }                            
                        }
                        form.IsQuestionSelected = activeForm.IsQuestionSelected;
                        form.IsLocationEnabled = activeForm.IsLocationEnabled;
                    }
                    else
                    {
                        form.Questions.Add(new QuestionsFormViewModel
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Category = item.Category,
                            SortOrderId = item.SortOrderId,
                            PossibleAnswers = possibleAnswerList
                        });
                        form.IsQuestionSelected = true;
                        form.IsLocationEnabled = true;
                    }                               
                }                

                var possibleObserverType = dbConnection.Query<dynamic>(
                    @"SELECT d.Id, d.Name AS Text
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'SOC' AND c.Name = 'Observer Title' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1").ToList();

                var possibleObserverTypeList = new List<ObserverType>();
                foreach (var pItem in possibleObserverType)
                {
                    possibleObserverTypeList.Add(new ObserverType { Id = pItem.Id, Name = pItem.Text });
                }
                form.ObserverType = possibleObserverTypeList;                

                return form;
            }
        }

        [HttpGet]
        public ActionResult Index()
        {
            ViewData["Message"] = "Fill your safety observation card";
            
            return View(GetSOCForm());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(SOCForm form)
        {
            foreach (var item in form.Questions)
            {
                if (!item.SelectedAnswer.HasValue)
                {
                    form.IsQuestionSelected = false;
                    break;                    
                }
                else
                {
                    form.IsQuestionSelected = true;
                }
            }

            if (form.IsQuestionSelected)
            {
                if (ModelState.IsValid)
                {
                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");

                    if (Request.Cookies["latitude"] != null && Request.Cookies["longitude"] != null &&
                        Request.Cookies["accuracy"] != null)
                    {
                        using (IDbConnection dbConnection = conn)
                        {
                            dbConnection.Open();
                            var observationType = conn.Query<string>(@"SELECT Name FROM Dictionary WHERE Id = @Id",
                                       new { @Id = form.SelectedObserverType.Value }).FirstOrDefault();

                            using (var trans = dbConnection.BeginTransaction())
                            {
                                try
                                {
                                    var socId = 0;
                                    socId = conn.Query<int>(@"INSERT INTO SOCDataV2 (Comments, Location, Observer, Task, ObservedBy, Latitude,
                                    Longitude, Accuracy, CreatedBy, CreatedDate, Timestamp)
                                    Values(@comments, @location, @observer, @task, @observedBy, @latitude, @longitude, @accuracy, @createdBy, 
                                    @createdDate, @timestamp) 
                                    SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];", new
                                    {
                                        @comments = form.Comments,
                                        @location = form.Location,
                                        @observer = form.Observer,
                                        @task = form.Task,
                                        @observedBy = observationType,
                                        @latitude = Convert.ToDecimal(Request.Cookies["latitude"]),
                                        @longitude = Convert.ToDecimal(Request.Cookies["longitude"]),
                                        @accuracy = Convert.ToInt32(Request.Cookies["accuracy"]),
                                        @createdBy = this.User.Identity.Name,
                                        @createdDate = DateTimeOffset.Now,
                                        @timestamp = DateTimeOffset.Now
                                    }, trans).SingleOrDefault();

                                    foreach (var item in form.Questions)
                                    {
                                        dbConnection.Execute(@"INSERT INTO SOCDataItemV2 (SOCDataId, QuestionId, AnswerId, CreatedBy,
                                        CreatedDate, Timestamp)
                                        Values(@SOCDataId, @QuestionId, @AnswerId, @CreatedBy, @CreatedDate, @Timestamp)", new
                                        {
                                            @SOCDataId = socId,
                                            @QuestionId = item.Id,
                                            @AnswerId = item.SelectedAnswer,
                                            @createdBy = this.User.Identity.Name,
                                            @createdDate = DateTimeOffset.Now,
                                            @timestamp = DateTimeOffset.Now
                                        }, trans);
                                    }

                                    trans.Commit();
                                    conn.Close();

                                    Response.Cookies.Delete("latitude");
                                    Response.Cookies.Delete("longitude");
                                    Response.Cookies.Delete("accuracy");

                                    return RedirectToAction("Index", "Home");
                                }
                                catch (Exception ex)
                                {
                                    trans.Rollback();
                                    conn.Close();
                                    return RedirectToAction("Error", ex);
                                }
                            }
                        }
                    }
                    else
                    {
                        form.IsLocationEnabled = false;
                        return View(GetSOCForm(form));
                    }
                }
                else
                {
                    return View(GetSOCForm(form));
                }
            }
            else
            {
                return View(GetSOCForm(form));
            }                      
        }

        [HttpGet]
        [Route("SOC/History")]
        public ActionResult History()
        {
            ViewData["Message"] = "Safety observation card logs";

            List<SOCLog> logs = new List<SOCLog>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var logList = dbConnection.Query<SOCLog>(@"SELECT Id, CreatedBy, CreatedDate, Location
                    FROM SOCDataV2
                    WHERE IsDeleted = 0
                    ORDER BY CreatedDate DESC");

                foreach (var item in logList)
                {
                    logs.Add(new SOCLog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        CreatedDate = item.CreatedDate
                    });
                }

                return View(logs);
            }
        }

        [HttpGet]
        [Route("SOC/History/{id}")]
        public ActionResult LogCard(int id)
        {
            ViewData["Message"] = "Safety observation card logs";

            SOCViewModel soc = new SOCViewModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();

                var socData = dbConnection.Query<SOCViewModel>(@"SELECT Comments, Location, Observer, CreatedDate, 
                    Task, ObservedBy, CreatedBy
                    FROM SOCDataV2 
                    WHERE IsDeleted = 0 AND Id = @Id", new { @Id = id }).FirstOrDefault();
                soc = socData;

                var questions = dbConnection.Query<QuestionsViewModel>(@"SELECT Id, QuestionId, AnswerId 
                    FROM SOCDataItemV2 
                    WHERE SOCDataId = @SOCDataId", new { @SOCDataId = id });
                soc.Questions = new List<QuestionsViewModel>();

                foreach (var item in questions)
                {
                    var question = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @questionId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @questionId = item.QuestionId }).FirstOrDefault();

                    var answer = dbConnection.Query<dynamic>(
                    @"SELECT d.Name
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @answerId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @answerId = item.AnswerId }).FirstOrDefault();

                    soc.Questions.Add(new QuestionsViewModel
                    {
                        Id = item.Id,
                        Name = question.Name,
                        Category = question.Category,
                        SelectedAnswer = answer.Name
                    });
                }
                
                return View(soc);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult History(string query)
        {
            ViewData["Message"] = "Safety observation card logs";

            List<SOCLog> logs = new List<SOCLog>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var logList = dbConnection.Query<SOCLog>(@"SELECT Id, CreatedBy, CreatedDate, Location
                    FROM SOCDataV2
                    WHERE IsDeleted = 0 AND (Location like @Query OR Task like @Query OR ObservedBy like @Query)
                    ORDER BY CreatedDate DESC", new { @Query = "%" + query + "%" });

                foreach (var item in logList)
                {
                    logs.Add(new SOCLog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        CreatedDate = item.CreatedDate
                    });
                }

                return View(logs);
            }
        }
    }
}