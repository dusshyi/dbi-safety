﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DBI.Safety.Models;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data.SqlClient;
using System.Data;
using System.Dynamic;

namespace DBI.Safety.Controllers
{
    public class HomeController : Controller
    {
        private string connectionString;
        public HomeController()
        {
            connectionString = @"Data Source=s0077;Initial Catalog=Safety;User Id=AssetTrack; Password=patrick#123;";
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public IActionResult Index()
        {
            ViewData["Message"] = "Welcome to Safety forms";

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
