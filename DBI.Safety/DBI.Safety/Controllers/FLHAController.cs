﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using DBI.Safety.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.Data.Common;

namespace DBI.Safety.Controllers
{
    public class FLHAController : Controller
    {
        private readonly IConfiguration configuration;
        public FLHAController(IConfiguration config)
        {
            this.configuration = config;
        }

        public enum YesOrNoEnum
        {
            Yes,
            No  
        }

        [HttpGet("FLHA/Step1")]
        public IActionResult Index()
        {
            ViewData["Message"] = "Step 1 - Fill your field level hazard assessment";

            FLHAStep1Form form = new FLHAStep1Form();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                form.EnvironmentalHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Environmental Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                form.ErgonomicHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Ergonomic Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                form.AccessAndOperationalHazards = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Access-Egress/ Operational Hazards' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                return View(form);
            }
        }

        [HttpPost("FLHA/Step1")]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FLHAStep1Form form)
        {
            foreach (var item in form.AccessAndOperationalHazards)
            {
                if(item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            foreach (var item in form.EnvironmentalHazards)
            {
                if (item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            foreach (var item in form.ErgonomicHazards)
            {
                if (item.Selected)
                {
                    form.IsHazardSelected = true;
                }
            }

            if (form.IsHazardSelected != null && (bool)form.IsHazardSelected)
            {
                Response.Cookies.Delete("step1");
                IDictionary<string, int> dict = new Dictionary<string, int>();
                foreach (var item in form.EnvironmentalHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                foreach (var item in form.ErgonomicHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                foreach (var item in form.AccessAndOperationalHazards)
                {
                    if (item.Selected)
                    {
                        dict.Add(item.Name, item.Id);
                    }
                }

                Response.Cookies.Append("step1", JsonConvert.SerializeObject(dict));
                return RedirectToAction("Step2");
            }
            else
            {
                form.IsHazardSelected = false;
                return View(form);
            }
        }

        [HttpGet("FLHA/Step2")]
        public IActionResult Step2()
        {
            ViewData["Message"] = "Continue fill your field level hazard assessment";            

            FLHAStep2Form form = new FLHAStep2Form();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                List<FLHADictionaryViewModel> severityList = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Severity' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                List<FLHADictionaryViewModel> probabilityList = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    INNER JOIN Form f ON c.FormId = f.Id
                    WHERE f.Name = 'FLHA' AND c.Name = 'Probability' AND 
                    d.IsActive = 1 AND c.IsActive = 1 AND f.IsActive = 1
                    ORDER BY d.SortOrderId"
                ).ToList();

                var severity = new List<SelectListItem>();
                foreach (var item in severityList)
                {
                    severity.Add(new SelectListItem { Value = item.Id, Text = item.Name.ToString(), Description = item.Description.ToString() });
                }

                severity.Insert(0, new SelectListItem()
                {
                    Value = 0,
                    Text = "Please choose severity"
                });

                var probability = new List<SelectListItem>();
                foreach (var item in probabilityList)
                {
                    probability.Add(new SelectListItem { Value = item.Id, Text = item.Name.ToString(), Description = item.Description.ToString() });
                }

                probability.Insert(0, new SelectListItem()
                {
                    Value = 0,
                    Text = "Please choose probability"
                });

                JObject step1Form = JObject.Parse(Request.Cookies["step1"]);
                IList<int> step1List = step1Form.Root.Select(c => (int)c).ToList();

                List<HazardForm> hazardForms = new List<HazardForm>();
                foreach (var item in step1List)
                {
                    var hazard = dbConnection.Query<FLHADictionaryViewModel>(
                    @"SELECT d.Id, d.Name, d.Description, c.Name AS Category, d.SortOrderId
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id 
                    WHERE d.Id IN (@hazardList) AND d.IsActive = 1 AND c.IsActive = 1
                    ORDER BY d.SortOrderId",
                    new { @hazardList = new[] { item } }).FirstOrDefault();

                    hazardForms.Add(new HazardForm
                    {
                        HazardId = hazard.Id,
                        Name = hazard.Name,
                        Title = hazard.Category,
                        Severity = severity,
                        Probability = probability
                    });
                }

                form.HazardForms = hazardForms;
                form.SeverityInfo = severity.Where(x => x.Value != 0);
                form.ProbabilityInfo = probability.Where(x => x.Value != 0);

                var yesOrNo = new List<YesOrNo>();
                yesOrNo.Add(new YesOrNo { IsSelected = true, Name = YesOrNoEnum.Yes.ToString() });
                yesOrNo.Add(new YesOrNo { IsSelected = false, Name = YesOrNoEnum.No.ToString() });
                form.YesOrNo = yesOrNo;

                form.IsLocationEnabled = true;

                return View(form);
            }
        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Step2(FLHAStep2Form form)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");

            if (Request.Cookies["latitude"] != null && Request.Cookies["longitude"] != null &&
                Request.Cookies["accuracy"] != null)
            {
                using (IDbConnection dbConnection = conn)
                {
                    dbConnection.Open();
                    using (var trans = dbConnection.BeginTransaction())
                    {
                        try
                        {
                            var flhaId = 0;
                            flhaId = conn.Query<int>(@"INSERT INTO FLHAData ([WorkInfo], [Location], [MusterPoint], [PermitJobNumber], [PPEInspected], 
                            [IsToolsEquipmentCompleted], [IsWarningRibbonNeeded], [IsWorkerWorkingAlone], [WorkerComment], Latitude, Longitude, 
                            Accuracy, [CreatedBy], [CreatedDate], [Timestamp])
                            Values(@workInfo, @location, @musterPoint, @permitJobNumber, @ppeInspected, @isToolsEquipmentCompleted, @isWarningRibbonNeeded, 
                            @isWorkerWorkingAlone, @workerComment, @latitude, @longitude, @accuracy, @createdBy, @createdDate, @timestamp) 
                            SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];", new
                            {
                                @workInfo = form.Work,
                                @location = form.Location,
                                @musterPoint = form.MusterPoint,
                                @permitJobNumber = form.PermitJob,
                                @ppeInspected = form.PPEInspected,
                                @isToolsEquipmentCompleted = form.IsToolsAndEquipmentCompleted,
                                @isWarningRibbonNeeded = form.IsWarningRibbonNeeded,
                                @isWorkerWorkingAlone = form.IsWorkerWorkingAlone,
                                @workerComment = form.Explain,
                                @latitude = Convert.ToDecimal(Request.Cookies["latitude"]),
                                @longitude = Convert.ToDecimal(Request.Cookies["longitude"]),
                                @accuracy = Convert.ToInt32(Request.Cookies["accuracy"]),
                                @createdBy = this.User.Identity.Name,
                                @createdDate = DateTimeOffset.Now,
                                @timestamp = DateTimeOffset.Now
                            }, trans).SingleOrDefault();

                            foreach (var item in form.HazardForms)
                            {
                                dbConnection.Execute(@"INSERT INTO FLHAEliminateAndControlData ([FLHADataId], [HazardId], [SeverityId], 
                                [ProbabilityId], [Priority], [Plans], [CreatedBy], [CreatedDate], [Timestamp])
                                Values(@flhaDataId, @hazardId, @severityId, @probabilityId, @priority, @plans, @createdBy, @createdDate, @timestamp)", new
                                {
                                    @flhaDataId = flhaId,
                                    @hazardId = item.HazardId,
                                    @severityId = item.SelectedSeverity,
                                    @probabilityId = item.SelectedProbability,
                                    @priority = item.Priority,
                                    @plans = item.Plans,
                                    @createdBy = this.User.Identity.Name,
                                    @createdDate = DateTimeOffset.Now,
                                    @timestamp = DateTimeOffset.Now
                                }, trans);
                            }

                            trans.Commit();
                            conn.Close();
                            return RedirectToAction("Index", "Home");
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            conn.Close();
                            return RedirectToAction("Error", ex);
                        }
                    }
                }
            }
            else
            {
                form.IsLocationEnabled = false;
                return View(form);
            }
        }

        public ActionResult History()
        {
            ViewData["Message"] = "Field level hazard assessment logs";

            List<FLHALog> logs = new List<FLHALog>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var logList = dbConnection.Query<FLHALog>(@"SELECT Id, CreatedBy, CreatedDate, Location
                    FROM FLHAData
                    WHERE IsDeleted = 0
                    ORDER BY CreatedDate DESC");

                foreach (var item in logList)
                {
                    logs.Add(new FLHALog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        CreatedDate = item.CreatedDate
                    });
                }

                return View(logs);
            }
        }

        [HttpGet]
        [Route("FLHA/History/{id}")]
        public ActionResult LogCard(int id)
        {
            ViewData["Message"] = "Field level hazard assessment logs";

            FLHAViewModel flha = new FLHAViewModel();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var flhaData = dbConnection.Query<FLHAViewModel>(@"SELECT *
                    FROM FLHAData WHERE IsDeleted = 0 AND Id = @Id", new { @Id = id }).FirstOrDefault();
                flha = flhaData;

                var hazards = dbConnection.Query<HazardViewModel>(@"SELECT Id, HazardId, SeverityId, ProbabilityId,
                    [Priority], Plans FROM FLHAEliminateAndControlData 
                    WHERE FLHADataId = @FLHADataId", new { @FLHADataId = id });
                flha.Hazards = new List<HazardViewModel>();

                foreach (var item in hazards)
                {
                    var hazard = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @hazardId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @hazardId = item.HazardId }).FirstOrDefault();

                    var severity = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @severityId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @severityId = item.SeverityId }).FirstOrDefault();

                    var probability = dbConnection.Query<dynamic>(
                    @"SELECT d.Name, c.Name AS Category
                    FROM Dictionary d
                    INNER JOIN Category c ON d.CategoryId = c.Id
                    WHERE d.Id = @probabilityId AND d.IsActive = 1 AND c.IsActive = 1",
                    new { @probabilityId = item.ProbabilityId }).FirstOrDefault();

                    flha.Hazards.Add(new HazardViewModel
                    {
                        Id = item.Id,
                        Name = hazard.Name,
                        Title = hazard.Category,
                        Severity = severity.Name,
                        SeverityDesc = severity.Category,
                        Probability = probability.Name,
                        ProbabilityDesc = probability.Category,
                        Priority = item.Priority,
                        Plans = item.Plans
                    });
                }

                return View(flha);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult History(string query)
        {
            ViewData["Message"] = "Safety observation card logs";

            List<FLHALog> logs = new List<FLHALog>();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            using (IDbConnection dbConnection = conn)
            {
                dbConnection.Open();
                var logList = dbConnection.Query<FLHALog>(@"SELECT Id, CreatedBy, CreatedDate, Location
                    FROM FLHAData
                    WHERE IsDeleted = 0 AND (Location like @Query OR WorkInfo like @Query)
                    ORDER BY CreatedDate DESC", new { @Query = "%" + query + "%" });

                foreach (var item in logList)
                {
                    logs.Add(new FLHALog
                    {
                        Id = item.Id,
                        CreatedBy = item.CreatedBy,
                        Location = item.Location,
                        CreatedDate = item.CreatedDate
                    });
                }

                return View(logs);
            }
        }
    }
}